# Project: Create Embeddings From Statmt News Corpus
This project aim is to automated the process of pulling data from repo for the provided language
and create embeddings from it after doing some preprocessings

To clean and prepare embedding from downloaded data use
```
bash train.sh {language name-i.e ru, de}
```
To download and do the rest use 
```
bash download_and_train.sh {language name-i.e ru, de}
```

### train.sh
It does the following steps
```
* unzip the data
* clean and prepare data for embedding
* create embedding
```
