#!/bin/bash

if [ $# -ne 1 ]; then
    echo $0: usage: provide ln name 
    exit 1
fi
ln=$1
mkdir ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2007.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2008.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2009.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2010.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2011.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2012.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2013.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2014.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2015.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2016.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2017.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2018.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus
wget http://data.statmt.org/news-crawl/${ln}/news.2019.${ln}.shuffled.deduped.gz -P ${ln}_news_corpus

echo All downloaded

chmod +x train.sh

bash train.sh ${ln}
