#!/bin/sh
if [ $# -ne 1 ]; then
    echo $0: usage: provide ln name 
    exit 1
fi

ln=$1

echo $ln
echo ${ln}
gunzip ${ln}_news_corpus/*.gz ${ln}_news_corpus/
echo "Unzipping Done"
which python
# conda activate ner
python prepare_data_for_gensim.py ${ln}
echo "Preprocessing Done"
sudo rm -r ${ln}_news_corpus/*.gz
python train_gensim_w2v.py ${ln} 
echo "Training Done"
