import gensim
import logging
import pickle
import time
import threading
import glob
import sys
# logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

log_dir = ''

import logging.config
logging.config.dictConfig({
      'version': 1,
          'disable_existing_loggers': True,
          })

logging.basicConfig(filename='{}training_embedding_status.log'.format(log_dir), filemode='a', format='%(name)s - %(levelname)s - %(threadName)s - %(message)s', level=logging.INFO)



def load_and_train(filename):
    with open(filename, 'rb') as fp:
        sentences = pickle.load(fp)
    train_embedding(filename, sentences)

def load_full_and_train(filenames):
    sentences = []

    for filename in filenames:
        with open(filename, 'rb') as fp:
            sentences += pickle.load(fp)
    filename = combined_embedding_name
    train_embedding(filename, sentences)


def train_embedding(filename, sentences):
    start_time = time.time()
    

    model = gensim.models.Word2Vec(sentences, size=300, window=10, min_count=5, workers=2048, iter=13)

    model.wv.save_word2vec_format(model_save_dir_bin + '{}_300_model.bin'.format(filename), binary=True)

    model.wv.save_word2vec_format(model_save_dir_txt + '{}_300_model.txt'.format(filename), binary=False)

    end_time = time.time()
    log_message = 'training done {} file {} elapsed time {}'.format(ln.upper(), filename, end_time - start_time)
    logging.info(log_message)
    print('elapsed time for {} {}'.format(filename, end_time - start_time))
    print('done')


def start_mutlti_threaded_training():
  threads = []
  
  for filename in filenames:
      x = threading.Thread(target=load_and_train, args=(filename,))
      threads.append(x)
      x.start()
  for idx, thread in enumerate(threads):
      thread.join()
      print('{} no thread joined '.format(idx))


def start_merged_files_training():
  load_full_and_train(filenames)
#    # load_and_train(filename)

# filenames.append('prothom_alo')
# load_full_and_train(filenames)

#x = threading.Thread(target=load_full_and_train, args=(filenames,))
#x.start()
#threads.append(x)

if __name__ == '__main__':
  if len(sys.argv) > 1:
    ln = sys.argv[1]

    
  
    file_dir = '{}_news_corpus/prepared_data_for_embedding/'.format(ln)
    filenames= glob.glob(file_dir + '*.pkl')
    filenames.sort()
    
    start_year = filenames[0].split('/')[2].split('-')[0]
    end_year  = filenames[-1].split('/')[2].split('-')[0]
    combined_embedding_name = '{}-{}-{}'.format(ln, start_year, end_year)
    logging.info('Start merging files and training for ln {} embedding name {} time {}'.format(ln.upper(), combined_embedding_name, time.time()))

    model_save_dir_bin = 'models/' + '{}/'.format(ln) + 'bin/'
    model_save_dir_txt = 'models/' + '{}/'.format(ln) + 'txt/'

    start_merged_files_training()


  else:
    print('provide ln name ')
  

print('all embedding done')
