import re
import pickle
import threading
import glob
import os
import sys
import time
import logging

count = 0
log_dir = ''

import logging.config
logging.config.dictConfig({
      'version': 1,
          'disable_existing_loggers': True,
          })

logging.basicConfig(filename='{}training_embedding_status.log'.format(log_dir), filemode='a', format='%(name)s - %(levelname)s - %(threadName)s - %(message)s', level=logging.INFO)



def prepare_data_file(filepath):
    data = []
    with open(filepath) as fp:
      lines = fp.readlines()
    for line in lines:
      words_in_line = list(filter(None, re.split(' ', line.strip())))
      data.append(words_in_line)

    print('samle data ', data[0])
    if not os.path.exists(file_dir + 'prepared_data_for_embedding'):
      os.mkdir(file_dir + 'prepared_data_for_embedding')
    with open(file_dir + 'prepared_data_for_embedding/{}_cleaned.pkl'.format(filepath.split('.')[1]+ '-' + filepath.split('.')[2]), 'wb') as fp:
        pickle.dump(data, fp)    
   



def start_preprocessing():
  threads = []

  for filepath in filepaths:
    print('file started {}'.format(filepath.split('.')[1]))
    #prepare_data_file(filepath)
    x = threading.Thread(target=prepare_data_file, args=(filepath,))
    threads.append(x)
    x.start()

  for thread in threads:
    thread.join()


if __name__ == '__main__':
  if len(sys.argv) > 1:
    
    ln = sys.argv[1]

    logging.info('start preprocessing for {}'.format(ln))
    start_time = time.time()

    file_dir = '{}_news_corpus/'.format(ln)
    filepaths = glob.glob(file_dir+ '*deduped')
    start_preprocessing()
    log_message = 'Preprocessing Done for ln {} elapsed time {}'.format(ln, time.time() - start_time)
    logging.info(log_message)




print('done')


